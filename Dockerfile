FROM nickgryg/alpine-pandas

RUN apk upgrade -U -a
RUN apk --no-cache add curl bash
RUN pip install --no-cache-dir 'pipenv==2018.11.26'
RUN mkdir -p /app /tmp /prod_dbs
COPY Pipfile Pipfile.lock /app/
WORKDIR /app
RUN pipenv install --system --skip-lock
COPY . /app
RUN chmod +x updater.sh

RUN touch /var/log/cron.log

ENV DATABASE "sqlite:////prod_dbs/covid-v2.sqlite"
ENV APP_DIR "/app"
ENV TMP_DIR "/tmp"
ENV PYTHON "python"
ENV PYTHONPATH "${PYTHONPATH}:/app"

RUN echo "*/10  *  *  *  * /bin/bash /app/updater.sh" > /etc/crontabs/root

CMD ["crond", "-l2", "-f"]
