#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail

TMP_DIR=${TMP_DIR:="./tmp"}
APP_DIR=${APP_DIR:="."}
PYTHON=${PYTHON:="pipenv run python"}

paths=( "https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto1/Covid-19.csv"
        "https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto3/CasosTotalesCumulativo.csv"
        # "https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto5/TotalesNacionales.csv"
        "https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto7/PCR.csv"
        "https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto8/UCI.csv"
        "https://raw.githubusercontent.com/MinCiencia/Datos-COVID19/master/output/producto14/FallecidosCumulativo.csv"
        "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"
        "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"
        "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv"
        "https://raw.githubusercontent.com/jorgeperezrojas/covid19-data/master/csv/encuesta_sochimi.csv" 
        "https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/testing/covid-testing-all-observations.csv" )

loads=( "src/minciencia/loader1.py -s False"
        "src/minciencia/loader3.py -s False"
        # "src/minciencia/loader5.py -s False"
        "src/minciencia/loader7.py -s False"
        "src/minciencia/loader8.py -s False"
        "src/minciencia/loader14.py -s False"
        "src/jhopkins/load_confirmed.py"
        "src/jhopkins/load_deaths.py"
        "src/jhopkins/load_recovered.py"
        "src/sochimi/loader.py"
        "src/owid/loader.py" )


function not_equal {
    if cmp -s $1 $2; then
        return 1
    else
        return 0
    fi
}

DB_PATH=${DATABASE:10}
cp ${DB_PATH} ${DB_PATH}.tmp
PREV_DATABASE=${DATABASE}
export DATABASE=${DATABASE}.tmp

for ((i=0; i<${#paths[*]}; i++));
do
    prev_file="${TMP_DIR}/$(basename ${paths[i]})"
    new_file="${prev_file}.tmp"
    wget -O ${new_file} ${paths[i]}
    if [ ! -f ${prev_file} ] || not_equal "${prev_file}" "${new_file}"; then
        echo "New version of ${new_file} detected, loading data"
        mv ${new_file} ${prev_file}
        cd ${APP_DIR} && ${PYTHON} ${loads[i]}
    fi
done

cp ${DB_PATH}.tmp ${DB_PATH}
export DATABASE=${PREV_DATABASE}
