# Datos Agregados COVID-19

El objetivo de este repositorio es generar una base de datos relacional con datos relacionados al COVID-19. Actualmente usamos los siguientes sets de datos:

 - [x] Confirmados, fallecidos y recuperados internacionales del [repositorio COVID-19 del CSSE de la Universidad Johns Hopkins](https://github.com/CSSEGISandData/COVID-19/)
 - [x] Datos nacionales del [repositorio el Ministerio de Ciencia del Gobierno de Chile](https://github.com/MinCiencia/Datos-COVID19/)
 - [x] Datos de la encuesta diaria de la [SOCHIMI](https://sochimi.cl) desde [el repositorio de Jorge Pérez](https://github.com/jorgeperezrojas/covid19-data/)
 - [x] Datos de PCRs reportados por países publicados en el [repositorio de Our World in Data](https://github.com/owid/covid-19-data/)


### Precaución!

Se ha dicho muchas veces que **la calidad de los datos oficiales no es buena**, recomendamos leer [esto](https://medium.com/@rbaeza_yates/datos-de-calidad-y-el-corona-virus-98893b7600e3) para entender mejor la situación y **NO hacer proyecciones** en base a estos datos. Este repositorio no contiene datos nuevos, el objetivo es exclusivamente disponer de los datos limpios y en un formato que permita hacer consultas complejas de forma más simple.

### Paneles

Esta base de datos la construimos para generar visualizaciones interactivas. Puedes revisarlas en **[covid19.imfd.cl](https://covid19.imfd.cl)** y ver más detalles en [este repositorio](https://gitlab.com/imfd-public/paneles-covid).

## Modelo de datos

Por el momento la base de datos guarda divisiones administrativas (países, regiones, estados, comunas, etc.) y para cada fecha sus distintas medidas (casos totales, muertes, recuperados, PCRs, Camas UCI, etc.). El modelo es muy simple pero permite guardar todo lo que necesitamos.


![data model](https://gitlab.com/imfd-public/aggregated-covid-data/-/raw/master/docs/data_model/data_model.png)


Pretendemos en un futuro guardar también períodos de cuarentena y otros datos que podrían requerir extender el modelo. Si quieres colaborar, crea tus merge requests!


## Comenzando 🚀

Estas instrucciones permiten generar esta misma base de datos en tu motor de bases de datos favorito. Han sido probadas en distintas distribuciones de GNU/Linux y debiesen funcionar sin mayor esfuerzo en macOS. Si usas Windows (pobre alma en desgracia) probablemente no funcione.

### Definir la base de datos.

La conexión a la base de datos se saca de la variable de entorno `DATABASE`, que debe ser una [URL de conexión](https://metacpan.org/pod/URI::db). Por ejepmlo para guardar estos datos en una base de datos Postgres definimos

```
export DATABASE=postgresql://user:password@localhost:5432/my_database
```
Por defecto el string es `sqlite:///db.sqlite`, lo que significa que la base de datos será un archivo [SQLite](https://sqlite.org/index.html) en la raíz del proyecto.

### Dependencias 📋

Para ejecutar las siguientes instrucciones necesitas tener instalado [Python3.8](https://www.python.org/downloads/) y [Pipenv](https://pypi.org/project/pipenv/). Para crear el ambiente virtual e instalar las dependencias simplemente clona el proyecto, entra en la carpeta y ejecuta
```
pipenv install
```

### Crear la base de datos ⚙️

Para sincronizar los repositorios externos, configurar inicialmente la base de datos y cargar los datos de las distintas fuentes, sólo tienes que ejecutar

```
make all
```

Esto va a crear un archivo `db.sqlite` en la raíz del proyecto que contiene todos los registros de las distintas fuentes.
Puedes especificar una ubicación distinta para la base de datos generada definiendo la variable de entorno `DB_FILE`.
Si quieres imprimir posibles inconsistencias y algo más de información define la variable de entorno `VERBOSE` como `"True"`.

Para comenzar desde cero usa
```
make clean
```

### Actualizando la base de datos
Si se actualizan los datos de las distintas fuentes, se puede actualizar la base de datos ejecutando:
```
make load_sources
```

Esto no solo va a agregar los datos nuevos a la base de datos, si no que también verifica si no hay datos antiguos que hayan sido modificados (lo que ha pasado más de una vez). Es por esto que el proceso puede ser algo lento, pero por ahora nada terrible (un par de minutos máximo).

## Estructura del Repositorio

El contenido del repositorio está distribuido de la siguiente manera:

- `src/sources`: repositorios de fuentes de datos.
- `src/database`: scripts que crean la base de datos vacía.
- `src/initializers`: scripts para inicializar la base de datos con datos de divisiones geopolíticas.
- `src/jhopkins`: scripts para agregar los datos de reportes internacionales.
- `src/minciencia`: scripts para agregar los datos de reportes nacionales.
- `docs/data_model`: codigo para generar el diagrama de la base de datos usando [dbdiagram](http://dbdiagram.io)


## Problemas y contribuciones

El repositorio es abierto para abrir issues y merge requests. Nosotros seguimos el [Gitlab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) pero entendemos que la mayoría de la gente está acostumbrada a [GitHub](https://www.github.com). De todas formas agradecemos si antes de empezar a trabajar en algo abren un issue y crean el merge request correspondiente.

# Autores

Este repositorio fue creado por [Jazmine Maldonado](https://gitlab.com/jazminemf), Felipe Cortés y [Martín Ugarte](https://martinugarte.com) del [Instituto Milenio Fundamentos de los Datos](http://www.imfd.cl).

## Repositorios externos

- [Datos de Chile (COVID19)](https://github.com/MinCiencia/Datos-COVID19/) - Ministerio de Ciencia, Tecnología, Conocimiento e Innovación, Gobierno de Chile.
- [Datos Internacionales (COVID19)](https://github.com/CSSEGISandData/COVID-19/) - Universidad de Johns Hopkins para Ciencia e Ingeniería de Sistemas
- [Datos de Divisiones Administrativas](https://gitlab.com/mugartec/geopolitical-data)
