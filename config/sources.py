# JOHN HOPKINS REPO

_JH_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master'
_JH_DATA = _JH_URL + '/csse_covid_19_data/csse_covid_19_time_series'

JH_LOOKUP_FILE = _JH_URL + '/csse_covid_19_data/UID_ISO_FIPS_LookUp_Table.csv'
JH_CONFIRMED = _JH_DATA + '/time_series_covid19_confirmed_global.csv'
JH_DEATHS = _JH_DATA + '/time_series_covid19_deaths_global.csv'
JH_RECOVERED = _JH_DATA + '/time_series_covid19_recovered_global.csv'

# GEOPOLICITAL DATA REPO
_GEO_URL = 'https://gitlab.com/mugartec/geopolitical-data/-/raw/master/data'

COUNTRIES_JSON = _GEO_URL + '/countries/countries.json'
REGIONS_JSON = _GEO_URL + '/chile/regions.json'
PROVINCES_JSON = _GEO_URL + '/chile/provinces.json'
COMMUNES_JSON = _GEO_URL + '/chile/communes.json'

# MINCIENCIA REPO
_MC_URL = 'https://raw.githubusercontent.com/MinCiencia/Datos-COVID19'
_MC_DATA = _MC_URL + '/master/output'

MC_P1_CSV = _MC_DATA + '/producto1/Covid-19.csv'
MC_P3_CSV = _MC_DATA + '/producto3/CasosTotalesCumulativo.csv'
MC_P5_CSV = _MC_DATA + '/producto5/TotalesNacionales.csv'
MC_P7_CSV = _MC_DATA + '/producto7/PCR.csv'
MC_P8_CSV = _MC_DATA + '/producto8/UCI.csv'
MC_P14_CSV = _MC_DATA + '/producto14/FallecidosCumulativo.csv'

# JPEREZ REPO
_JP_URL = "https://raw.githubusercontent.com/jorgeperezrojas/covid19-data"

SOCHIMI_CSV = _JP_URL + "/master/csv/encuesta_sochimi.csv"

# OWID REPO
_OWID_URL = "https://raw.githubusercontent.com/owid/covid-19-data/master/"

PCR_CSV = _OWID_URL + "public/data/testing/covid-testing-all-observations.csv"
