"""SOCHIMI poll data loader

This script will load data from the SOCHIMI daily poll as published in
https://github.com/jorgeperezrojas/covid19-data/blob/master/csv/encuesta_sochimi.csv

It accepts two options:
    -v --verb : bool
        Show info regarding the processing and missing divisions.
        Defaults to False
    -f --start-date: str
        Date to start adding information.
        It must be provided in dd/mm/yyyy format
        Defauls to 22/01/2020
"""


import argparse
import pandas

from datetime import datetime
from src.database import models
from src.database.models import Division, get_measurement_type
from config.sources import SOCHIMI_CSV


def load_data_from_df(df, chile, database, start_date, verb=False):
    """Load data from a timeseries dataframe to the database

    Parameters
    ----------
    df : pandas.DataFrame
        The initial dataframe form a JHopkins daily report
    chile : models.Division
        The database entry for Chile
    database : Pewee.db
        The database
    start_date : datetime.date
        In case we want to insert data only from a certain date
    verb : bool
        Verbose

    Returns
    -------
    counts : tuple
        (new entries, updated entries, total entries)
    """
    total, created, updated = 0, 0, 0
    for _, row in df.iterrows():
        data = []
        mt = get_measurement_type("Sochimi " + row[0])
        for c in df.columns[1:]:
            try:
                data.append({
                    "division": chile,
                    "date": datetime.strptime(c, "%Y-%m-%d").date(),
                    "measurement": row[c]
                })
            except Exception:
                pass

        # print(f'Processing {len(data)} rows for {mt}')
        result = models.DayMeasurement.insert_collection(data, mt, verb)
        total += result[2]
        created += result[0]
        updated += result[1]

    return created, updated, total


def main(start_date, verb=False):
    """ Load all data from timeseries csv to the database """
    print(f"Loading {SOCHIMI_CSV}")
    database = models.get_db()
    chile = Division.get(Division.code == 'CL')
    df = pandas.read_csv(SOCHIMI_CSV)
    result = load_data_from_df(df, chile, database, start_date, verb)
    print(f"\tTotal: {result[2]}\n\tNew: {result[0]}\n\tUpdated: {result[1]}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verb", default="False")
    parser.add_argument("-s", "--start_date", default="22/01/2020")
    args = parser.parse_args()

    try:
        start_date = datetime.strptime(args.start_date, "%d/%m/%Y").date()
    except Exception:
        # print(f"Date {args.start_date} not parsed, processing complete file")
        start_date = datetime.strptime("22/1/2020", "%d/%m/%Y").date()

    args.verb = args.verb.lower in ["true", "yes", "t", "y", "1"]
    print("\n------ Loading data from SOCHIMI poll ------\n")
    main(start_date, args.verb)
    print("\n------ DONE ------\n")
