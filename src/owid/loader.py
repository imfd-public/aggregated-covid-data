"""OWID PCR countries data loader

This script will load data of PCRs per day per country as published in
https://github.com/owid/covid-19-data/blob/master/public/
data/testing/covid-testing-all-observations.csv

It accepts two options:
    -v --verb : bool
        Show info regarding the processing and missing divisions.
        Defaults to False
    -f --start-date: str
        Date to start adding information.
        It must be provided in dd/mm/yyyy format
        Defauls to 22/01/2020
"""


import argparse
import pandas

from datetime import datetime
from src.database import models
from src.database.models import COUNTRY
from src.database.models import PCR
from src.database.models import get_measurement_type
from src.utils.name_maps import get_name_divisions_map, Translator
from config.sources import PCR_CSV


def load_data_from_df(df, name_map, measure, start_date, verb=False):
    """Load data from a timeseries dataframe to the database

    Parameters
    ----------
    df : pandas.DataFrame
        The initial dataframe form a JHopkins daily report
    name_map : dict
        A map to get the division for each row
    measure : MeasurementType
        The type of measurement that is being updated
    start_date : datetime.date
        In case we want to insert data only from a certain date
    verb : bool
        Verbose

    Returns
    -------
    counts : tuple
        (new entries, updated entries, total entries)
    """
    data = []
    for _, row in df.iterrows():
        key = (Translator.name_from_ISO3(row["ISO code"]), )
        if key not in name_map:
            if verb:
                print(f"\t{key} not found")
        else:
            day = datetime.strptime(row["Date"], '%Y-%m-%d').date()
            if day >= start_date:
                data.append({
                    "division": name_map[key],
                    "date": day,
                    "measurement": row["Daily change in cumulative total"]
                })

    return models.DayMeasurement.insert_collection(data, measure, verb)


def load_data(file_url, measure, start_date, verb=False):
    """ Load data from a timeseries csv to the database

    Parameters
    ----------
    file_url : str
        The url to the csv file to create the dataframe from
    measure : MeasurementType
        The type of measurement that is being updated
    start_date : datetime.date
        In case we want to insert data only from a certain date
    verb : bool
        Verbose

    Returns
    -------
    counts : tuple
        (new entries, updated entries, total entries)
    """
    df = pandas.read_csv(file_url)
    country_map = get_name_divisions_map(COUNTRY)
    n, u, t = load_data_from_df(df, country_map, measure, start_date, verb)
    print(f"{t} entries processed. {n} new and {u} updated.")


def main(start_date, verb=False):
    """ Load all data from timeseries csv to the database """

    print(f"Loading PCRs from {PCR_CSV}")
    pcr_type = get_measurement_type(PCR)
    load_data(PCR_CSV, pcr_type, start_date, verb)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verb", default="False")
    parser.add_argument("-s", "--start_date", default="22/01/2020")
    args = parser.parse_args()

    database = models.get_db()

    try:
        start_date = datetime.strptime(args.start_date, "%d/%m/%Y").date()
    except Exception:
        # print(f"Date {args.start_date} not parsed, processing complete file")
        start_date = datetime.strptime("22/1/2020", "%d/%m/%Y").date()

    args.verb = args.verb.lower in ["true", "yes", "t", "y", "1"]
    print("\n------ Loading PCR data from OWID repo ------\n")
    main(start_date, args.verb)
    print("\n------ DONE ------\n")
