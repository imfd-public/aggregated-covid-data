from fuzzywuzzy import process
from unidecode import unidecode
import src.database.models as models

NULL_MAPPINGS = {
    'NaN': None,
    'nan': None,
    '-': None
}


def check_null(value):
    """Check if value match with some None equivalence in NULL_MAPPINGS dictionary

    Parameters
    ----------
    value : str
        value to check

    Returns
    -------
    value : str or None
        The value if there is not none equivalent, or None if it is.
    """
    return NULL_MAPPINGS[value] if value in NULL_MAPPINGS else value


def normalize(value):
    """Normalize the value removing accented and lower case characters and removing
    unnecessary spaces.

    Parameters
    ----------
    value : str
        value to check

    Returns
    -------
    value : str or None
        The normalized value.
    """
    if value != value:
        return None
    if isinstance(value, str):
        return check_null(unidecode(' '.join(value.lower().strip().split())))
    return value


def get_the_most_similar(value, list_of_values, exact_match=False):
    """Get the most similar value in a list of values.

    Parameters
    ----------
    value : str
        value to search
    list_of_values : list(str)
        list of values to compare with
    exact_match : bool
        (optional) to indicate if it is allowed to search similar values or only equal
        values
        Defaults to False

    Returns
    -------
    similar_value : str or None
        The most similar value in the list of values, or None if there is not similar
        match with a score over 75 (treshhold).
    """
    if exact_match:
        return value if value in list_of_values else None
    else:
        ratio = process.extractOne(value, list_of_values)
        if ratio:
            return ratio[0] if ratio[1] > 75 else None
        return None


def get_divisions_dict(type, database):
    """Get a dictionary with a subset of divisions filtered by type using
    normalized name as key.

    Parameters
    ----------
    type: str
        valid models.DivisionType name
    database: str
        name of the database

    Returns
    -------
    divisions_dict : dict
        dictionary with a subset of models.Division filtered by type using
        normalized name as key
    """

    db = models.get_db()

    divisions = models.Division.select().join(
        models.DivisionType
    ).where(
        models.DivisionType.name == type
    )
    divisions_dict = {}
    for division in divisions:
        divisions_dict[normalize(division.name)] = division

    db.close()
    return divisions_dict


def get_similar_from_mapping(value, mapping, approved_matches, rejected_matches, safe):
    """Get the most similar key in a dictionary and returns its value. It asks for
    confirmation before return the most similar value and update approved_matches or
    rejected_matches according to user responses. If safe is true, return automatically
    without asking for approval.

    Parameters
    ----------
    value : str
        value to search
    mapping : dict
        dictionary with strings as key
    approved_matches : dict
        dict with historial of similarity replacements approved by the user
    rejected_matches : dict
        dict with historial of similarity replacements rejected by the user
    safe : bool
        Indicates if it should ask for confirmation before to return similar key values

    Returns
    -------
    similar_key_value : any
        The value of the most similar key in the dictionary or None if there is
        not similar match.
    """
    if value is None:
        return None

    # check exact match
    if value in mapping:
        return mapping[value]

    # check saved user actions
    elif value in approved_matches:
        return mapping[approved_matches[value]]
    elif value in rejected_matches:
        return None

    # find the most similar and ask for approval
    else:
        similar_value = get_the_most_similar(
            value, list(mapping.keys())
        )
        if similar_value and safe:
            input_text = f'There is no exact match for "{value}".\n'
            input_text += f'Must we use "{similar_value}" instead?\n'
            input_text += '1) Yes\n'
            input_text += '2) Yes all the same\n'
            input_text += '3) No all the same\n'
            ans = input(input_text)
            if ans.lower().strip() in ['1', 'y', 'yes']:
                return mapping[similar_value]
            elif ans.lower().strip() in ['2', 'yes all the same']:
                approved_matches[value] = similar_value
                return mapping[similar_value]
            elif ans.lower().strip() in ['3', 'no all the same']:
                rejected_matches[value] = similar_value
        elif similar_value and not safe:
            approved_matches[value] = similar_value
            return mapping[similar_value]

        return None
