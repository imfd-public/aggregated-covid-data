"""Minciencia product 7 loader

This script will load Product 7 data from Ministerio de Ciencia into the relational
database. If the Commune or Region is written different between reports and database,
it uses the most similar match in database.

It accepts two options:
    -s --safe : bool
        True if wants the script to ask for confirmation before use similiar Communes or
        Regions names.
        Defaults to True

    -f --start-date: str
        Date to start adding information. It must be provided in dd/mm/yyyy format
        Defauls to 22/01/2020
"""

import pandas
import argparse
import src.database.models as models

from utils import normalize, get_divisions_dict, get_similar_from_mapping
from config.sources import MC_P7_CSV
from datetime import datetime

approved_matches = {}
rejected_matches = {}


def load_data(database, safe, start_date):
    """Load product 7 data from the time series in the csv file to the database

    Parameters
    ----------
    safe: bool
        True if wants the script to ask for confirmation before use similiar Communes or
        Regions names.
        Defaults to True
    start_date : datetime
        Date to start adding information
        Defauls to 22/01/2020
    """

    df = pandas.read_csv(MC_P7_CSV, sep=",", encoding="utf8")
    divisions_dict = get_divisions_dict(models.REGION, database)
    measurement_type = models.get_measurement_type(models.PCR)

    items = []
    for index, row in df.iterrows():
        division_name = normalize(row['Region'])
        division = get_similar_from_mapping(
            division_name,
            divisions_dict,
            approved_matches,
            rejected_matches,
            safe
        )

        if division is None:
            print(f'REVISION REQUIRED: Division name "{division_name}" not found.')
        else:
            days = []
            for c in df.columns[3:]:
                days.append((c, datetime.strptime(c, '%Y-%m-%d').date()))

            for day in [x for x in days if x[1] > start_date]:
                measurement = normalize(row[day[0]])
                if measurement:
                    items.append({
                        "division": division,
                        "date": day[1],
                        "measurement": measurement
                    })

    models.DayMeasurement.insert_collection(items, measurement_type)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--safe', default='True')
    parser.add_argument("-f", "--start_date", default="22/01/2020")
    args = parser.parse_args()

    database = models.get_db()

    try:
        start_date = datetime.strptime(args.start_date, "%d/%m/%Y").date()
    except Exception:
        print(f"Date {args.start_date} not parsed, processing complete file")
        start_date = datetime.strptime("22/01/2020", "%d/%m/%Y").date()

    print("\n------ Loading product 7 data from MinCyT repo ------\n")
    load_data(database, args.safe == 'True', start_date)
    print("\n------ DONE ------\n")
