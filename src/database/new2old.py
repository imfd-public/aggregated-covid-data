import argparse
import sqlite3
import os

from config.paths import ROOT_PATH
from src.database import models
from src.database.models import (
    DivisionType,
    Division,
    DayMeasurement)


OLD_SCHEMA = '''
CREATE TABLE "administrative_division_type" ("id" INTEGER NOT NULL PRIMARY KEY, "name" VARCHAR(255) NOT NULL);
CREATE TABLE "administrative_division" ("id" INTEGER NOT NULL PRIMARY KEY, "parent_id" INTEGER, "division_type_id" INTEGER, "name" VARCHAR(255) NOT NULL, "code" VARCHAR(255), "population" INTEGER, "lat" REAL, "lng" REAL, FOREIGN KEY ("parent_id") REFERENCES "administrative_division" ("id"), FOREIGN
KEY ("division_type_id") REFERENCES "administrative_division_type" ("id"));
CREATE INDEX "administrative_division_parent_id" ON "administrative_division" ("parent_id");
CREATE INDEX "administrative_division_division_type_id" ON "administrative_division" ("division_type_id");
CREATE UNIQUE INDEX "administrative_division_name_parent_id" ON "administrative_division" ("name", "parent_id");
CREATE TABLE "division_day_info" ("id" INTEGER NOT NULL PRIMARY KEY, "division_id" INTEGER NOT NULL, "date" DATE NOT NULL, "total_cases" INTEGER, "recovered_cases" INTEGER, "deaths" INTEGER, FOREIGN KEY ("division_id") REFERENCES "administrative_division" ("id"));
CREATE INDEX "division_day_info_division_id" ON "division_day_info" ("division_id");
CREATE UNIQUE INDEX "division_day_info_division_id_date" ON "division_day_info" ("division_id", "date");
CREATE TABLE "quarantined_division" ("id" INTEGER NOT NULL PRIMARY KEY, "division_id" INTEGER NOT NULL, "start" DATE NOT NULL, "end" DATE NOT NULL, FOREIGN KEY ("division_id") REFERENCES "administrative_division" ("id"));
CREATE INDEX "quarantined_division_division_id" ON "quarantined_division" ("division_id");
'''


def create_transfer_model_func(table_name, collection, transf_func):
    def inner(cursor):
        rows = [transf_func(item) for item in collection]
        n_columns = len(rows[0])
        q = ','.join(['?'] * n_columns)
        query = f'INSERT INTO {table_name} VALUES ({q})'
        cursor.executemany(query, rows)
    return inner


TRANSFER_FUNCS = [
    create_transfer_model_func(
        'administrative_division_type',
        DivisionType.select(),
        lambda div: (div.id, div.name)),
    create_transfer_model_func(
        'administrative_division',
        Division.select(),
        lambda div: (div.id, div.parent_id, div.division_type_id, div.name, div.code, div.population, div.lat, div.lng)),
]


INFOTYPE2COLNAME = {
    'Deaths': 'deaths',
    'Confirmed': 'total_cases',
    'Recovered': 'recovered_cases'
}


def transfer_day_info(cursor):
    inserted_entries = 0
    updated_entries = 0
    skipped_entries = 0
    for day_info in DayMeasurement.select():
        cursor.execute(
            'SELECT id FROM division_day_info WHERE division_id = ? AND date = ?',
            (day_info.division_id, day_info.date))
        division_day_info_id = cursor.fetchone()

        # Old schema didn't support some measurement types
        # We could add them but we're ignoring them for now
        if day_info.type.name not in INFOTYPE2COLNAME:
            skipped_entries += 1
            continue

        col_name = INFOTYPE2COLNAME[day_info.type.name]

        if division_day_info_id is None:
            inserted_entries += 1
            cursor.execute(
                'INSERT INTO division_day_info(division_id, date, {}) VALUES (?,?,?)'.format(col_name),
                (day_info.division_id,
                 day_info.date,
                 day_info.measurement))
        else:
            updated_entries += 1
            cursor.execute(
                'UPDATE division_day_info SET {} = ? WHERE id = ?'.format(col_name),
                (day_info.measurement, division_day_info_id[0]))
    print(f'Inserted Entries: {inserted_entries}')
    print(f'Updated Entries : {updated_entries}')
    print(f'Skipped Entries : {skipped_entries}')


if __name__ == '__main__':
    default_output_db = os.path.join(ROOT_PATH, "db_old.sqlite")
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_database", default=default_output_db)
    args = parser.parse_args()

    database = models.get_db()

    connection = sqlite3.connect(args.output_database)
    cursor = connection.executescript(OLD_SCHEMA)

    for f in TRANSFER_FUNCS:
        f(cursor)

    transfer_day_info(cursor)
    connection.commit()
