import inspect
import sys
import os

from playhouse.db_url import connect
from peewee import (
    Model,
    ForeignKeyField,
    DateField,
    CharField,
    IntegerField,
    FloatField
)

_DATABASE = connect(os.environ.get('DATABASE') or 'sqlite:///db.sqlite')

CITY = "City"
COUNTRY = "Country"
STATE = "State"
PROVINCE = "Province"
COMMUNE = "Commune"
REGION = "Region"

DEATHS = "Deaths"
CONFIRMED = "Confirmed"
RECOVERED = "Recovered"
ACTIVE = "Active"
PCR = "PCR"
UCI = "UCI"


def get_db(day=None):
    return _DATABASE


class BaseModel(Model):
    class Meta:
        database = _DATABASE
        legacy_table_names = False


class DivisionType(BaseModel):
    name = CharField(null=False)

    def __str__(self):
        return f"[{self.name} ({self.id})]"


class Division(BaseModel):
    parent = ForeignKeyField('self', null=True, backref='divisions')
    division_type = ForeignKeyField(
        DivisionType,
        null=True,
        backref='divisions'
    )
    name = CharField(null=False)
    code = CharField(null=True)
    population = IntegerField(null=True)
    lat = FloatField(null=True)
    lng = FloatField(null=True)

    class Meta:
        indexes = ((('name', 'parent'), True),)

    def __str__(self):
        return f"[{self.name} ({self.code})]"

    @classmethod
    def insert_new_by_code(cls, collection, verbose=False):
        codes_set = set([x["code"] for x in collection])
        query = cls.select(cls.code).where(cls.code.in_(codes_set))
        existing = set([x.code for x in query])
        code_map = {x["code"]: x for x in collection}
        new_objects = [code_map[x] for x in codes_set - existing]
        if verbose:
            print(f"Creating {len(new_objects)} divisions")
        cls.insert_many(new_objects).execute()

    @classmethod
    def insert_new_by_name(cls, collection, verbose=False):
        names_set = set([x["name"] for x in collection])
        query = cls.select(cls.name).where(cls.name.in_(names_set))
        existing = set([x.name for x in query])
        name_map = {x["name"]: x for x in collection}
        new_objects = [name_map[x] for x in names_set - existing]
        if verbose:
            print(f"Creating {len(new_objects)} divisions")
        cls.insert_many(new_objects).execute()


class MeasurementType(BaseModel):
    name = CharField(null=False)

    def __str__(self):
        return f"[{self.name} ({self.id})]"


class DayMeasurement(BaseModel):
    division = ForeignKeyField(Division, backref='measurements', null=False)
    date = DateField(null=False)
    type = ForeignKeyField(MeasurementType, backref='measurements', null=False)
    measurement = IntegerField(null=True)

    class Meta:
        indexes = ((('division', 'date', 'type'), True),)

    def __str__(self):
        return f"Info for {self.division.name} on {self.date}"

    @classmethod
    def insert_collection(cls, collection, measurement_type, verbose=False):
        existing = cls.select().where(
            cls.division.in_([x["division"] for x in collection]),
            cls.date.in_([x["date"] for x in collection]),
            cls.type == measurement_type
        )

        existing_map = {(x.division, x.date): x for x in existing}
        col_map = {(x["division"], x["date"]): x for x in collection}

        to_create = []
        to_update = []

        for key, value in col_map.items():
            if key not in existing_map:
                value['type'] = measurement_type
                to_create.append(value)
                existing_map[key] = value
            else:
                prev = existing_map[key]
                if prev.measurement is None:
                    prev.measurement = value['measurement']
                    to_update.append(prev)
                elif value['measurement'] != prev.measurement and verbose:
                    print(f"CONFLICT: {prev} in database, {value} on input")

        if to_create:
            cls.insert_many(to_create).execute()
        if to_update:
            cls.bulk_update(to_update, ['measurement'])

        return len(to_create), len(to_update), len(collection)


def get_models_list():
    module = sys.modules[__name__]
    classes = [x[1] for x in inspect.getmembers(module, inspect.isclass)]
    models = [x for x in classes if issubclass(x, BaseModel)]
    return [x for x in models if x != BaseModel]


def get_division_type(div_string):
    result, _ = DivisionType.get_or_create(name=div_string)
    return result


def get_measurement_type(measurement_string):
    result, _ = MeasurementType.get_or_create(name=measurement_string)
    return result
