import models


if __name__ == "__main__":
    db = models.get_db()

    print(f"\n------ Creating {db} ------\n")
    tables = models.get_models_list()
    table_names = "\n\t" + "\n\t".join([x.__name__ for x in tables])

    print(f"Adding tables: {table_names}")
    db.create_tables(tables)
    db.close()

    print("\n------ DONE ------\n")
