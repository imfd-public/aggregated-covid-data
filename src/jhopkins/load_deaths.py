"""JHopkins daily reports loader

This script will load deaths from Johns Hopkins daily reports into the
relational database.

It accepts two options:
    -l --last : bool
        Load only last file
        Defaults to False, if True it only loads data from the last report

    -v --verb : bool
        Show info regarding the processing and missing divisions.
        Defaults to False
"""


import argparse

from datetime import datetime
from src.jhopkins.utils import load_data
from src.database.models import DEATHS
from src.database.models import get_measurement_type
from config.sources import JH_DEATHS


def main(start_date, verb=False):
    """ Load deaths data from timeseries csv to the database """

    print(f"Processing {JH_DEATHS}")
    deaths_type = get_measurement_type(DEATHS)
    result = load_data(JH_DEATHS, deaths_type, start_date, verb)

    print(f"\tTotal: {result[2]}\n\tNew: {result[0]}\n\tUpdated: {result[1]}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verb", default="False")
    parser.add_argument("-s", "--start_date", default="22/01/2020")
    args = parser.parse_args()

    try:
        start_date = datetime.strptime(args.start_date, "%d/%m/%Y").date()
    except Exception:
        # print(f"Date {args.start_date} not parsed, processing complete file")
        start_date = datetime.strptime("22/1/2020", "%d/%m/%Y").date()

    args.verb = args.verb.lower in ["true", "yes", "t", "y", "1"]
    print("\n------ Loading deaths cases from JH repo ------\n")
    main(start_date, args.verb)
    print("\n------ DONE ------\n")
