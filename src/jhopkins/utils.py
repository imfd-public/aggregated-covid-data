import numpy as np
import pandas
from datetime import datetime
from src.database import models
from src.database.models import STATE, COUNTRY
from src.utils.name_maps import get_name_divisions_map, Translator


def get_dataframes_and_keys(df):
    """Get all the dataframes and their related keys from a DataFrame

    Parameters
    ----------
    df : pandas.DataFrame
        The original DataFrame

    Returns
    -------
    dataframe_maps : tuple({"df": pandas.DataFrame, "keys": tuple(str)})
        Each dictionary contains a dataframe corresponding to a single division
        type (country or state), and a tuple with the keys that will be used to
        search for existing elements.
    """
    countries = {"df": df[df[STATE].isnull()], "keys": (COUNTRY,)}
    states = {
        "df": df[df[STATE].notnull()],
        "keys": (STATE, COUNTRY)
    }
    return (countries, states)


def rename_columns(df):
    """Rename the columns to COUNTRY and STATE from models"""
    rename_map = {"Country/Region": COUNTRY, "Province/State": STATE}
    df.rename(columns=rename_map, inplace=True)


def clean_dataframe(df):
    """Clean a dataframe corresponding to a JHopkins daily report.
    The dataframe is modified inplace so no return value.

    Parameters
    ----------
    df : pandas.Dataframe
        The dataframe
    """
    rename_columns(df)
    df = df.drop(df[df[STATE] == "Recovered"].index)
    return df


def aggregate_countries(df):
    """Modify the dataframe to aggregate the content of those countries
    that have only one row per province state. See here for details:
    https://gitlab.com/imfd-public/aggregated-covid-data/-/merge_requests/45

    Parameters
    ----------
    df : pandas.Dataframe
        The dataframe
    """

    # Get those countries that only have province/state rows.
    no_state = set(df[df[STATE].isnull()][COUNTRY].values)
    with_state = set(df[df[STATE].notnull()][COUNTRY].values)
    countries = with_state - no_state

    for c in countries:
        # Create the new row first
        c_df = df[df[COUNTRY] == c]
        new_row = dict(c_df.iloc[0])
        new_row[STATE] = np.NaN
        measurements_df = c_df.filter(regex=r'\d{1,2}/\d{1,2}/\d{2,4}', axis=1)
        for column in measurements_df.columns:
            new_row[column] = measurements_df[column].sum()
        df = df.append(new_row, ignore_index=True)

    return df


def load_data_from_df(df, keys, name_map, measure, start_date, verb=False):
    """Load data from a timeseries dataframe to the database

    Parameters
    ----------
    df : pandas.DataFrame
        The initial dataframe form a JHopkins daily report
    keys : tuple
        The keys to use for the name_map to get each division
    name_map : dict
        A map to get the division for each row
    measure : MeasurementType
        The type of measurement that is being updated
    start_date : datetime.date
        In case we want to insert data only from a certain date
    verb : bool
        Verbose

    Returns
    -------
    counts : tuple
        (new entries, updated entries, total entries)
    """
    data = []
    for _, row in df.iterrows():
        key = tuple(Translator.eng_to_es(row[k]) for k in keys)
        if key not in name_map:
            if verb:
                print(f"\t{key} not found")
        else:
            days = []
            for c in df.columns[4:]:
                try:
                    days.append((c, datetime.strptime(c, "%m/%d/%y").date()))
                except Exception:
                    pass

            for day_str, day in [x for x in days if x[1] > start_date]:
                if not pandas.isna(row[day_str]):
                    data.append({
                        "division": name_map[key],
                        "date": day,
                        "measurement": row[day_str]
                    })

    return models.DayMeasurement.insert_collection(data, measure, verb)


def load_data(file_url, measure, start_date, verb=False):
    """ Load data from a timeseries csv to the database

    Parameters
    ----------
    file_url : str
        The url to the csv file to create the dataframe from
    measure : MeasurementType
        The type of measurement that is being updated
    start_date : datetime.date
        In case we want to insert data only from a certain date
    verb : bool
        Verbose

    Returns
    -------
    counts : tuple
        (new entries, updated entries, total entries)
    """
    df = pandas.read_csv(file_url)
    df = clean_dataframe(df)
    df = aggregate_countries(df)

    s_keys = (STATE, COUNTRY)
    s_map = get_name_divisions_map(*s_keys)
    s_df = df[df[STATE].notnull()]

    c_keys = (COUNTRY,)
    c_map = get_name_divisions_map(*c_keys)
    c_df = df[df[STATE].isnull()]

    r1 = load_data_from_df(s_df, s_keys, s_map, measure, start_date, verb)
    r2 = load_data_from_df(c_df, c_keys, c_map, measure, start_date, verb)
    return r1[0] + r2[0], r1[1] + r2[1], r1[2] + r2[2]
