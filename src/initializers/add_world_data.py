import requests
import pandas

from src.database import models
from config.sources import JH_LOOKUP_FILE, COUNTRIES_JSON
from src.database.models import CITY, STATE, COUNTRY
from src.database.models import Division
from src.utils.name_maps import get_name_divisions_map, Translator


def add_countries_data(df):
    countries_json = requests.get(COUNTRIES_JSON).json()
    countries = df[df.Province_State.isnull() & df.iso2.notnull()]

    country_type = models.get_division_type(COUNTRY)

    country_data = []
    for _, row in countries.iterrows():
        country_data.append({
            'name': countries_json[row.iso2]["es"],
            'code': row.iso2,
            'lat': row.Lat,
            'lng': row.Long_,
            'division_type': country_type,
            'population': row.Population
        })

    print(f"Creating {len(country_data)} countries")
    Division.insert_new_by_name(country_data)


def add_states_data(df):
    country_map = get_name_divisions_map(COUNTRY)

    state_type = models.get_division_type(STATE)
    states_df = df[df.Province_State.notnull() & df.Admin2.isnull()]
    state_data = []
    for _, row in states_df.iterrows():
        if row.Country_Region != 'Chile':
            parent = country_map[(Translator.eng_to_es(row.Country_Region),)]
            state_data.append({
                'name': row.Province_State,
                'code': row.UID,
                'lat': row.Lat,
                'lng': row.Long_,
                'division_type': state_type,
                'population': row.Population,
                'parent': parent
            })

    print(f"Creating {len(state_data)} states")
    Division.insert_new_by_code(state_data)


def add_cities_data(df):
    city_type = models.get_division_type(CITY)
    state_map = get_name_divisions_map(STATE, COUNTRY)

    cities_df = df[df.Admin2.notnull()]
    cities_data = []
    for _, row in cities_df.iterrows():
        cities_data.append({
            'name': row.Admin2,
            'code': row.UID,
            'lat': row.Lat,
            'lng': row.Long_,
            'division_type': city_type,
            'population': row.Population,
            'parent': state_map[
                (row.Province_State, Translator.eng_to_es(row.Country_Region))]
        })

    print(f"Creating {len(cities_data)} cities")
    Division.insert_new_by_code(cities_data)


def add_world_data(database):
    df = pandas.read_csv(JH_LOOKUP_FILE)
    df = df[df.Lat.notnull()]

    add_countries_data(df)
    add_states_data(df)
    add_cities_data(df)


if __name__ == "__main__":
    database = models.get_db()
    print("\n------ Creating divisions of the world ------\n")
    add_world_data(database)
    print("\n------ DONE ------\n")
