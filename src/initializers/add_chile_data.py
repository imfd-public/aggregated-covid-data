import requests

from src.database import models
from src.database.models import COMMUNE, COUNTRY, REGION, PROVINCE
from config.sources import REGIONS_JSON, PROVINCES_JSON, COMMUNES_JSON

CHILE = {
    "code": "CL",
    "name": "Chile",
    "lat": -35.6751,
    "lng": -71.543,
    "population": 19116209
}


def create_chile(database):
    country = models.get_division_type(COUNTRY)
    chile, created = models.Division.get_or_create(**CHILE)
    chile.division_type = country
    chile.save()

    if created:
        print(f"Created country {chile}")

    return chile


def create_divisions(database, div_json_url, parent_map, division_string):
    divisions_content = requests.get(div_json_url).json()

    division_type = models.get_division_type(division_string)
    division_data = []
    for division in divisions_content:
        division_data.append({
            'code': division['codigo'],
            'name': division.get('nombre_corto', division['nombre']),
            'lat': division['lat'],
            'lng': division['lng'],
            'population': division.get('poblacion'),
            'parent': parent_map[division['codigo_padre']],
            'division_type': division_type
        })
        # print(division_data[-1])

    models.Division.insert_new_by_code(division_data)


def get_next_map(previous_map, type_string):
    tp = models.get_division_type(type_string)
    divisions = models.Division.select().where(
        models.Division.parent.in_(list(previous_map.values())),
        models.Division.division_type == tp,
    )
    return {x.code: x for x in divisions}


def add_chile_data(database):
    chile = create_chile(database)

    print("Creating regions")

    country_map = {chile.code: chile}
    create_divisions(database, REGIONS_JSON, country_map, REGION)

    print("Creating provinces")

    region_map = get_next_map(country_map, REGION)
    create_divisions(database, PROVINCES_JSON, region_map, PROVINCE)

    print("Creating communes")

    province_map = get_next_map(region_map, PROVINCE)
    create_divisions(database, COMMUNES_JSON, province_map, COMMUNE)


if __name__ == "__main__":
    database = models.get_db()
    print("\n------ Creating divisions of Chile ------\n")
    add_chile_data(database)
    print("\n------ DONE ------\n")
