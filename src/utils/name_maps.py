import requests

from src.database import models
from src.database.models import Division
from config.sources import COUNTRIES_JSON


def get_name_divisions_map(tp=None, parent_tp=None):
    result = Division.select()

    if tp is not None:
        div_type = models.get_division_type(tp)
        result = result.where(Division.division_type == div_type)

    if parent_tp is None:
        return {(x.name,): x for x in result}

    parent_type = models.get_division_type(parent_tp)
    Parent = Division.alias()
    result = result.join(Parent, on=(Division.parent == Parent.id))
    result = result.where(Parent.division_type == parent_type)
    return {(x.name, x.parent.name): x for x in result}


class Translator():
    ctrs_json = requests.get(COUNTRIES_JSON).json()
    eng_es = {v["en"]: v["es"] for v in ctrs_json.values() if "en" in v}
    iso_es = {v["iso3"]: v["es"] for v in ctrs_json.values() if "en" in v}

    @staticmethod
    def eng_to_es(english_name):
        return Translator.eng_es.get(english_name, english_name)

    @staticmethod
    def name_from_ISO3(iso3):
        return Translator.iso_es.get(iso3, iso3)
