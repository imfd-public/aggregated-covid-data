PYTHON = pipenv run python
DB_FILE = db.sqlite
DATABASE ?= sqlite:///${DB_FILE}
VERBOSE ?= "False"
OLD_SCHEMA_DB_FILE ?= db_old.sqlite

all: create_db load_sources

create_db:
	$(PYTHON) src/database/create_db.py
	$(PYTHON) src/initializers/add_chile_data.py
	$(PYTHON) src/initializers/add_world_data.py

load_sources:
	$(PYTHON) src/minciencia/loader1.py -s False
	$(PYTHON) src/minciencia/loader3.py -s False
	# $(PYTHON) src/minciencia/loader5.py -s False
	$(PYTHON) src/minciencia/loader7.py -s False
	$(PYTHON) src/minciencia/loader8.py -s False
	$(PYTHON) src/minciencia/loader14.py -s False
	$(PYTHON) src/jhopkins/load_confirmed.py -v "$(VERBOSE)" -s "$(JH_START_DATE)"
	$(PYTHON) src/jhopkins/load_deaths.py -v "$(VERBOSE)" -s "$(JH_START_DATE)"
	$(PYTHON) src/jhopkins/load_recovered.py -v "$(VERBOSE)" -s "$(JH_START_DATE)"
	$(PYTHON) src/sochimi/loader.py -s False
	$(PYTHON) src/owid/loader.py -v "$(VERBOSE)" -s "$(JH_START_DATE)"

$(OLD_SCHEMA_DB_FILE): $(DF_FILE)
	$(PYTHON) src/database/new2old.py -o "$(OLD_SCHEMA_DB_FILE)"

generate-old-db: $(OLD_SCHEMA_DB_FILE)

clean:
	rm -f "$(DB_FILE)"
	rm -f "$(OLD_SCHEMA_DB_FILE)"
